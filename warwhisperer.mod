<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="warwhisperer" version="0.15.2" date="18/08/2010" >

		<Author name="smurfy" email="" />
		<Description text="simple whisper helper" />
        <VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
		<Dependencies>
			<Dependency name="EASystem_LayoutEditor" />
            <Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />			
			<Dependency name="EA_ChatWindow" />
			<Dependency name="LibSlash" />
		</Dependencies>

		<Files>
				<File name="warwhisperer.xml" />
	            <File name="LibStub.lua" />
        	    <File name="LibGUI.lua" />	       	    
	       	    <File name="ww_default_specialchannels.lua" />
	       	    <File name="warwhisperer.lua" />
		</Files>
		<SavedVariables>
			<SavedVariable name="WarWhisperer.Settings"/>
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="warwhisperer.OnInitialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
			<CallFunction name="warwhisperer.OnShutdown" />
		</OnShutdown>

	</UiMod>
</ModuleFile>

	