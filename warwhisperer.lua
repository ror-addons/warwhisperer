warwhisperer = {}
warwhisperer.debug = false
warwhisperer.conversations = {}
warwhisperer.lastconversations = {}
warwhisperer.logdisplay = {}
warwhisperer.active = L"";

--3
-- Wothor was here ;P

if not WarWhisperer then WarWhisperer = {} end
local WarWhisperer = WarWhisperer

local LibGUI = LibStub("LibGUI")

local TextLogGetNumEntries = TextLogGetNumEntries
local TextLogCreate = TextLogCreate
local TextLogClear = TextLogClear
local TextLogLoadFromFile = TextLogLoadFromFile
local TextLogAddEntry = TextLogAddEntry
local TextLogGetEntry = TextLogGetEntry

-- patterns created by Sylvanaar & used in Prat
-- and wim from world of warcraft
local urlpatterns = {
	-- X@X.Y url (---> email)
	"^(www%.[%w_-]+%.%S+[^%p%s])",
	"%s(www%.[%w_-]+%.%S+[^%p%s])",
	-- XXX.YYY.ZZZ.WWW:VVVV/UUUUU url
	"^(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?:%d%d?%d?%d?%d?/%S+[^%p%s])",
	"%s(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?:%d%d?%d?%d?%d?/%S+[^%p%s])",
	-- XXX.YYY.ZZZ.WWW:VVVV url (IP of ts server for example)
	"^(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?:%d%d?%d?%d?%d?)", 
	"%s(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?:%d%d?%d?%d?%d?)", 
	-- XXX.YYY.ZZZ.WWW/VVVVV url (---> IP)
	"^(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?/%S+[^%p%s])", 
	"%s(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?/%S+[^%p%s])", 
	-- XXX.YYY.ZZZ.WWW url (---> IP)
	"^(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?)", 
	"%s(%d%d?%d?%.%d%d?%d?%.%d%d?%d?%.%d%d?%d?)",
	-- X.Y.Z:WWWW/VVVVV url
	"^([%w_.-]+[%w_-]%.%a%a+:%d%d?%d?%d?%d?/%S+[^%p%s])", 
	"%s([%w_.-]+[%w_-]%.%a%a+:%d%d?%d?%d?%d?/%S+[^%p%s])", 
	-- X.Y.Z:WWWW url  (ts server for example)
	"^([%w_.-]+[%w_-]%.%a%a+:%d%d?%d?%d?%d?)", 
	"%s([%w_.-]+[%w_-]%.%a%a+:%d%d?%d?%d?%d?)", 
	-- X.Y.Z/WWWWW url
	"^([%w_.-]+[%w_-]%.%a%a+/%S+[^%p%s])", 
	"%s([%w_.-]+[%w_-]%.%a%a+/%S+[^%p%s])", 
	-- X.Y.Z url
	"^([%w_.-]+[%w_-]%.%a%a+)", 
	"%s([%w_.-]+[%w_-]%.%a%a+)", 
	-- X://Y url
	"(%a+://[%d%w_-%.]+[%.%d%w_%-%/%?%%%=%;%:%+%&]*)", 
};	

-- Our main window object
local W_SETTINGS
local W_URLCOPY

local firstLoad = true
local test_num = 0
local FLASH_DELAY = 0.5
local timetoflash = FLASH_DELAY
local flashstat = 0

function warwhisperer.OnInitialize()
	
    -- Sanity check, we have LibGUI right?
    if not LibGUI then
        TextLogAddEntry("Chat", 0,L"warwhisperer failed to load a working instance of LibGUI and thus is unable to run.")
        return
    end
	
	-- LibSlash check, a required dependency
	if (LibSlash == nil) then
		TextLogAddEntry("Chat", 0,L"Warning: warwhisperer couldn't find LibSlash!")
		return
	end	
	
	-- check to see if slash command "/ww" is available for planb
	if LibSlash.IsSlashCmdRegistered("ww") then
		TextLogAddEntry("Chat", 0,L"Warning: something else seems to be using /ww - no room for warwhisperer.")
		return
	end     
	if LibSlash.IsSlashCmdRegistered("wwconf") then
		TextLogAddEntry("Chat", 0,L"Warning: something else seems to be using /wwconf - no room for warwhisperer.")
		return
	end     	
	
	-- register slash command "/ww"
	LibSlash.RegisterSlashCmd("ww", function(input) warwhisperer.Command(input) end)	
	LibSlash.RegisterSlashCmd("wwconf", function() warwhisperer.ShowConfig() end)	
	
	warwhisperer.conversations={}

	if not WarWhisperer.Settings then WarWhisperer.Settings = {} end
		
	if (WarWhisperer.Settings.mwwidth == nil) then
		WarWhisperer.Settings.mwwidth = 400
	end		
	
	if (WarWhisperer.Settings.mwheight == nil) then
		WarWhisperer.Settings.mwheight = 250
	end		

	if (WarWhisperer.Settings.mwposx == nil) then
		WarWhisperer.Settings.mwposx = 10
	end	
	
	if (WarWhisperer.Settings.mwposy == nil) then
		WarWhisperer.Settings.mwposy = 10
	end	

	if (WarWhisperer.Settings.mwshow == nil) then
		WarWhisperer.Settings.mwshow = false
	end	
	
	if (WarWhisperer.Settings.mwalpha == nil) then
		WarWhisperer.Settings.mwalpha = 100
	end		
	
	if (WarWhisperer.Settings.queryuser == nil) then
		WarWhisperer.Settings.queryuser = true
	end
	
	if (WarWhisperer.Settings.nocombatpopup == nil) then
		WarWhisperer.Settings.nocombatpopup = false
	end	
	
	if (WarWhisperer.Settings.flashtab == nil) then
		WarWhisperer.Settings.flashtab = false
	end		
	
	if (WarWhisperer.Settings.font == nil) then
		WarWhisperer.Settings.font = 5
	end	
	
	if (WarWhisperer.Settings.channelsdisplay == nil) then
		WarWhisperer.Settings.channelsdisplay = {}
	end		
	
	if WarWhisperer.Settings.point == nil then
		WarWhisperer.Settings.point = "topleft"
	end
	
	if WarWhisperer.Settings.relpoint == nil then
		WarWhisperer.Settings.relpoint = "topleft"
	end
	
	if WarWhisperer.Settings.relwin == nil then
		WarWhisperer.Settings.relwin = "Root"
	end
	
	if WarWhisperer.Settings.moveable == nil then
		WarWhisperer.Settings.moveable = false
	end
	
    warwhisperer.CreateGui()
	
    RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "warwhisperer.GotRX")
    RegisterEventHandler( SystemData.Events.CHAT_REPLY, "warwhisperer.GotReply")
    RegisterEventHandler( SystemData.Events.SOCIAL_SEARCH_UPDATED, "warwhisperer.getPlayerInfoUpdate")
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "warwhisperer.UpdateSettings" )
	RegisterEventHandler( SystemData.Events.SCENARIO_END, "warwhisperer.UpdateSettings" )
	RegisterEventHandler( SystemData.Events.GROUP_UPDATED, "warwhisperer.UpdateSettings" )
	RegisterEventHandler( SystemData.Events.GUILD_REFRESH, "warwhisperer.UpdateSettings" )
	if (WarWhisperer.Settings.flashtab) then
		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "warwhisperer.UpdateFlashing")
	end
	
	RegisterEventHandler(SystemData.Events.LOADING_END, "warwhisperer.SetHook")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "warwhisperer.SetHook")	
	table.insert(LayoutEditor.EventHandlers, warwhisperer.LayoutEditorDone)
		
	TextLogCreate("InternalWarWhispererHistoryHelper",6000)
	TextLogCreate("InternalWarWhispererTimeStampHelper",6001)
	
	TextLogAddEntry("Chat", 0, L"WarWhisperer loaded!");
end


function warwhisperer.initWarBoard()

	if (WarBoard ~= nil) then
		if (WarBoard.AddMod("WarBoard_WarWhisperer")) then
			LabelSetText("WarBoard_WarWhispererText",L"WarWhisperer")
		end
	end
end

function warwhisperer.SetIcon(dynamicImageTitle, icon)
	local texture, x, y = GetIconData(icon)
	DynamicImageSetTexture(dynamicImageTitle, texture, x, y)
end

function warwhisperer.OnShutdown()
	if (DoesWindowExist("warwhispererMain")) then
		WarWhisperer.Settings.mwwidth ,WarWhisperer.Settings.mwheight = WindowGetDimensions( "warwhispererMain" )
		--WarWhisperer.Settings.mwposx ,WarWhisperer.Settings.mwposy = WindowGetOffsetFromParent( "warwhispererMain" )	
		WarWhisperer.Settings.point,WarWhisperer.Settings.relpoint,WarWhisperer.Settings.relwin,WarWhisperer.Settings.mwposx,WarWhisperer.Settings.mwposy = WindowGetAnchor("warwhispererMain", 1)
		WarWhisperer.Settings.mwshow = WindowGetShowing("warwhispererMain")
		DestroyWindow("warwhispererMain")
	end	
	
	if (DoesWindowExist("warwhispererMainInner")) then
		DestroyWindow("warwhispererMainInner")
	end		
end

function warwhisperer.UpdateSettings()
 warwhisperer.UpdateWindows()
 warwhisperer.UpdateTabs()  
end

function warwhisperer.ShowConfig()
	W_SETTINGS:Show()
end

function warwhisperer.UpdateFlashing(elapsed)  
	timetoflash = timetoflash - elapsed
    if timetoflash > 0 then
        return
    end
    timetoflash = FLASH_DELAY

	local n = 0
	flashstat = not flashstat	
	for _,obj in ipairs(warwhisperer.conversations) do
		n=n+1
		if (obj.marked == 1) then
			if flashstat then
				ButtonSetTextColor("warwhispererMainTab"..n, 0, 255, 0,0)
			else
				ButtonSetTextColor("warwhispererMainTab"..n, 0, 222, 192,50)
			end
		end
	end
end

function warwhisperer.Command(input)
	--if no input, toggle active and make sure page is set correctly and return
	if ( string.find(input, "%S") == nil ) then
		warwhisperer.ShowChat(nil, true)
		return
	end
	
	-- condense multiple spaces into one
	input = string.gsub(input, "(%s+)", " ")	
		
	-- loop over the strings in the parameter list
	local n = 0
	local user = L""
	local text = L""
    
    -- Because string.gmatch is broken in 1.3.1 (copied from squared, credits by aiiane)
    local gmatch = function( str, pattern )
        local init = 1
        local function gmatch_it()
            if init <= str:len() then 
                local s, e = str:find(pattern, init)
                if s then
                    local res = {str:match(pattern, init)}
                    init = e+1
                    return unpack(res)
                end
            end
        end
        return gmatch_it
    end    
    
	for substring in gmatch(input, "([%w%p=]+)") do
		if (n == 0) then
			user = StringToWString(string.match(substring, "(%w+)=?"))
		else
			text = text..StringToWString(string.match(substring, "(%w+)=?"))..L" "
		end
		n = n + 1

	end
	
	warwhisperer.AddConversation(user,1)
	warwhisperer.ShowChat(user, true)
    
	--hide default chatwindow reply thingy
    WindowAssignFocus( "EA_TextEntryGroupEntryBoxTextInput", false )
    WindowSetShowing( "EA_TextEntryGroupEntryBox", false )
	
	-- show ours
	d("WindowAssignFocus1")
	WindowAssignFocus("warwhispererMainSendBox",true)	

	if (text ~= L"") then
		TextEditBoxSetText("warwhispererMainSendBox", text)
		--we should send here bu it somehow does not work :(
	end
end

function warwhisperer.LayoutEditorDone(event)
	if (LayoutEditor.EDITING_END ~= event) then return end
	
	warwhisperer.UpdateWindows()	
end

function warwhisperer.UpdateWindows()
	local mainscale = WindowGetScale("warwhispererMain")
    local mainx,mainy = WindowGetDimensions( "warwhispererMain" )

	WarWhisperer.Settings.mwwidth ,WarWhisperer.Settings.mwheight = WindowGetDimensions( "warwhispererMain" )
	--WarWhisperer.Settings.mwposx ,WarWhisperer.Settings.mwposy = WindowGetOffsetFromParent( "warwhispererMain" )	
	WarWhisperer.Settings.point,WarWhisperer.Settings.relpoint,WarWhisperer.Settings.relwin,WarWhisperer.Settings.mwposx,WarWhisperer.Settings.mwposy = WindowGetAnchor("warwhispererMain", 1)
	WarWhisperer.Settings.mwshow = WindowGetShowing("warwhispererMain")
	
	local innerwidth = (mainx-16)/0.8;
	local innerheight = (mainy-70)/0.8;
	
	--tab container
	WindowSetDimensions("warwhispererMainTabContainer", mainx-55,33)	
		
	WindowSetAlpha ( "warwhispererMain", WarWhisperer.Settings.mwalpha / 100)		
	--close button
	WindowSetScale("warwhispererMainClose",mainscale*0.7)
	WindowSetScale("warwhispererMainConfig",mainscale*0.7)
	WindowSetScale("warwhispererMainTabLeft",mainscale*0.7)
	WindowSetScale("warwhispererMainTabRight",mainscale*0.7)
	--sendbox
	WindowSetDimensions("warwhispererMainSendBox", innerwidth,32)	
	WindowSetScale("warwhispererMainSendBox",mainscale*0.8)
		--chat
	WindowSetScale("warwhispererMainInner",mainscale*0.8)
	WindowSetDimensions("warwhispererMainInner", innerwidth,innerheight)	
--	WindowClearAnchors("warwhispererMainInner")
--	WindowAddAnchor("warwhispererMainInner", "top", "warwhispererMain", "top", 0, mainscale*33)
	--tabs
	for i=1, 100, 1 do
		if (DoesWindowExist("warwhispererMainTab"..i)) then
			WindowSetScale("warwhispererMainTab"..i,mainscale*0.6)
		end
	end	
	
    WarWhisperer.Settings.font = W_SETTINGS.EditFont:SelectedIndex()
    LogDisplaySetFont("warwhispererMainInnerText",ChatSettings.Fonts[WarWhisperer.Settings.font].fontName)
    warwhisperer.updateSendText()
end

function warwhisperer.updateSendText() 
    local CurChatChannel = 0
	for key,obj in ipairs(warwhisperer.conversations) do
		if (warwhisperer.active == obj.name) then
			if (obj.conv_type == 1) then
				CurChatChannel = SystemData.ChatLogFilters.TELL_SEND
			end
			
			if (obj.conv_type == 99) then
				   for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
						if (L"<"..channelobj.title..L">" == obj.name) then
							CurChatChannel = channelobj.outputFilter
						end
				  end
		   end

            local channelColor = ChatSettings.ChannelColors[CurChatChannel]
            TextEditBoxSetTextColor ("warwhispererMainSendBox", channelColor.r, channelColor.g, channelColor.b)
			break
		end
	end	
end

function warwhisperer.SelectOutputChannel() 
	for key,obj in ipairs(warwhisperer.conversations) do
		if (warwhisperer.active == obj.name) then
			if (obj.conv_type == 99) then
                for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
                    if (L"<"..channelobj.title..L">" == obj.name) then
                        local inputnum = table.getn(channelobj.inputFilter)	
                        if (inputnum > 1) then                    
                            EA_Window_ContextMenu.CreateContextMenu("warwhispererContextMenuChannel")
                            for filterkey, filter in pairs(channelobj.inputFilter) do
                                local butstate = false
                                if (channelobj.outputFilter == filter) then
                                    butstate = true
                                end
                                EA_Window_ContextMenu.AddMenuItem(
                                            ChatSettings.Channels[filter].name,
                                            warwhisperer.SelectOutputChannelDo, 
                                            butstate, 
                                            true)
                                --ButtonSetTextColor(EA_Window_ContextMenu.activeWindow, 255,0,0)
                            end
                            EA_Window_ContextMenu.Finalize()
                        end
                    end
              end
              
		   end
           break
		end
	end	
end

function warwhisperer.SelectOutputChannelDo()
    local name = ButtonGetText(SystemData.ActiveWindow.name)
	for key,obj in ipairs(warwhisperer.conversations) do
		if (warwhisperer.active == obj.name) then
			if (obj.conv_type == 99) then
                for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
                    if (L"<"..channelobj.title..L">" == obj.name) then
                        local inputnum = table.getn(channelobj.inputFilter)	
                        if (inputnum > 1) then                    
                            for filterkey, filter in pairs(channelobj.inputFilter) do
                                if ChatSettings.Channels[filter].name == name then
                                    channelobj.outputFilter = filter
                                    warwhisperer.updateSendText() 
                                    break
                                end
                            end
                        end
                    end
              end
		   end
           break
		end
	end	
end

function warwhisperer.ToggleLastTabs()
	EA_Window_ContextMenu.CreateContextMenu("warwhispererContextMenu")

	EA_Window_ContextMenu.AddMenuItem(
				L"Open Configuration", 
				warwhisperer.ShowConfig, 
				WindowGetShowing("warwhispererMainSettings"), 
				true)	

	for key,obj in ipairs(warwhisperer.lastconversations) do
		if (obj.last) then
			EA_Window_ContextMenu.AddMenuItem(
						obj.name,
						warwhisperer.AddUserTab, 
						false, 
						true)
		end
	end		

   for key,obj in pairs(warwhisperer_defaultchannels) do
		if (WarWhisperer.Settings.channelsdisplay[obj.name] ~= nil) and (WarWhisperer.Settings.channelsdisplay[obj.name] == true) then	
			EA_Window_ContextMenu.AddMenuItem(
						L"<"..obj.title..L">",
						warwhisperer.AddSpecialTab, 
						false, 
						true)
		end
	end

	EA_Window_ContextMenu.Finalize()
end

function warwhisperer.AddUserTab()
	local name = ButtonGetText(SystemData.ActiveWindow.name)
	warwhisperer.AddConversation(name,1)
	warwhisperer.ShowChat(name)
end

function warwhisperer.AddSpecialTab()
	local name = ButtonGetText(SystemData.ActiveWindow.name)
	for key,obj in pairs(warwhisperer_defaultchannels) do
		if (WarWhisperer.Settings.channelsdisplay[obj.name] ~= nil) and (WarWhisperer.Settings.channelsdisplay[obj.name] == true) then	
			if (name == L"<"..obj.title..L">") then
				warwhisperer.AddConversation(name,99,obj)
				warwhisperer.ShowChat(name)
				return
			end
		end
	end

end

function warwhisperer.CreateGui()
 	CreateWindow("warwhispererMain", false)
	
	ButtonSetText("warwhispererMainConfigActive", L"+")
	
    WindowClearAnchors("warwhispererMain")
	local mainscale = WindowGetScale("warwhispererMain")
    --WindowAddAnchor("warwhispererMain", "topleft", "Root", "topleft", WarWhisperer.Settings.mwposx * mainscale, WarWhisperer.Settings.mwposy * mainscale)
	WindowAddAnchor("warwhispererMain", WarWhisperer.Settings.point, WarWhisperer.Settings.relwin, WarWhisperer.Settings.relpoint, WarWhisperer.Settings.mwposx, WarWhisperer.Settings.mwposy)
	WindowSetDimensions("warwhispererMain", WarWhisperer.Settings.mwwidth,WarWhisperer.Settings.mwheight)
	WindowSetMovable("warwhispererMain", WarWhisperer.Settings.moveable)
	--CreateWindow("warwhispererMainInner", false)
		
	local texture, x, y = GetIconData( 2 )
	DynamicImageSetTexture( "warwhispererMainInnerTextCareerButtonIconBase", texture, x, y )		
	
	local texture, x, y = GetIconData( EA_Window_Macro.MACRO_ICONS_ID_BASE + 14)
	DynamicImageSetTexture( "warwhispererMainInnerTextGroupInviteButtonIconBase", texture, x, y ) 		
	
	local texture, x, y = GetIconData( EA_Window_Macro.MACRO_ICONS_ID_BASE + 69)
	DynamicImageSetTexture( "warwhispererMainInnerTextAddFriendButtonIconBase", texture, x, y ) 	

	local texture, x, y = GetIconData( EA_Window_Macro.MACRO_ICONS_ID_BASE + 91)
	DynamicImageSetTexture( "warwhispererMainInnerTextAddIgnoreButtonIconBase", texture, x, y ) 		
	
	local texture, x, y = GetIconData( EA_Window_Macro.MACRO_ICONS_ID_BASE + 114)
	DynamicImageSetTexture( "warwhispererMainInnerTextTargetButtonIconBase", texture, x, y ) 	
	
	LogDisplaySetShowTimestamp( "warwhispererMainInnerText", false ) -- don't use the embedded timestamp
	
	--Register with the layout editor.
	LayoutEditor.RegisterWindow( "warwhispererMain", L"WarWhisperer", L"The main window of WarWhisperer",true, true, false, nil)	
		
	WindowSetShowing("warwhispererMain",WarWhisperer.Settings.mwshow);
	
	WindowSetShowing("warwhispererMainInnerTextScrollbar",false); --DEBUG

	-- URL COPY UI
	W_URLCOPY =  LibGUI("Blackframe", "warwhispererMainURLCopy")
    W_URLCOPY:MakeMovable()
    W_URLCOPY:Popable(false)
    W_URLCOPY:AnchorTo("Root", "center", "center", 0, 0)
    W_URLCOPY:Resize(280, 75)

    local e
    
    -- Title label
    e = W_URLCOPY("Label")
    e:Resize(250)
    e:AnchorTo(W_URLCOPY, "top", "top", 0, 3)
    e:Font("font_clear_medium")
    e:SetText(L"Copy Url")
	e:IgnoreInput()
    W_URLCOPY.Title = e		
	
	-- text
	e = W_URLCOPY("textbox")
    e:Resize(250)
    e:AnchorTo(W_URLCOPY, "top", "top", 0, 35)	
	W_URLCOPY.TextBox = e	
	
    -- close button
    e = W_URLCOPY("Closebutton")
	W_URLCOPY.CloseButton = e
    W_URLCOPY.CloseButton.OnLButtonUp =
        function()
            W_URLCOPY:Hide()
        end		
	
	W_URLCOPY:Hide()
	
	-- Settings UI
	W_SETTINGS = LibGUI("Blackframe", "warwhispererMainSettings")
    W_SETTINGS:MakeMovable()
    W_SETTINGS:Popable(false)
    W_SETTINGS:AnchorTo("Root", "right", "right", -200, -100)
    W_SETTINGS:Resize(280, 510)
        
    -- Title label
    e = W_SETTINGS("Label")
    e:Resize(250)
    e:AnchorTo(W_SETTINGS, "top", "top", 0, 3)
    e:Font("font_clear_large")
    e:SetText(L"WarWhisperer 0.15")
	e:IgnoreInput()
    W_SETTINGS.Title = e	
	
    e = W_SETTINGS("Label")
    e:Resize(250)
    e:AnchorTo(W_SETTINGS, "top", "top", 0, 20)
    e:Font("font_clear_small")
    e:SetText(L"Configuration")
	e:IgnoreInput()
    W_SETTINGS.SubTitle = e		

    -- 'Opacity' label
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 5)
    e:Align("leftcenter")
	e:Font("font_clear_small")
    e:SetText(L"Opacity:")
    W_SETTINGS.LabelOpacity = e
 	
	-- Opacity sliders
    e = W_SETTINGS("Slider")
    e:AnchorTo(W_SETTINGS.LabelOpacity, "bottom", "top", 0, 0)
    e:SetRange(0,100)
	--e:RegisterEvent("OnSlide")
    e.OnLButtonUp =
        function()
             WarWhisperer.Settings.mwalpha = W_SETTINGS.SliderOpacity:GetValue()
			 warwhisperer.UpdateSettings()
			 warwhisperer.UpdateWindows()
        end		
	e:SetValue(WarWhisperer.Settings.mwalpha)
    W_SETTINGS.SliderOpacity = e
	
    -- 'queryUser' label
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 70)
    e:Align("leftcenter")
	e:Font("font_clear_small")
    e:SetText(L"Query for userinfos:")
    W_SETTINGS.LabelQueryUser = e
    
    -- 'queryUser' Checkbox
    e = W_SETTINGS("Checkbox")
    e:AnchorTo(W_SETTINGS.LabelQueryUser, "right", "left", 0, 0)
    e.OnLButtonUp =
        function()
             WarWhisperer.Settings.queryuser = W_SETTINGS.CheckQueryUser:GetValue()
        end		
	e:SetValue(WarWhisperer.Settings.queryuser)
    W_SETTINGS.CheckQueryUser = e		
	
    -- 'combatpopup' label
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 95)
    e:Align("leftcenter")
	e:Font("font_clear_small")
    e:SetText(L"No popup during combat:")
    W_SETTINGS.LabelCombat = e
    
    -- 'combatpopup' Checkbox
    e = W_SETTINGS("Checkbox")
    e:AnchorTo(W_SETTINGS.LabelCombat, "right", "left", 0, 0)
    e.OnLButtonUp =
        function()
             WarWhisperer.Settings.nocombatpopup = W_SETTINGS.CheckCombat:GetValue()
        end		
	e:SetValue(WarWhisperer.Settings.nocombatpopup)
    W_SETTINGS.CheckCombat = e			
	
    -- 'flashtab' label
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 120)
    e:Align("leftcenter")
	e:Font("font_clear_small")
    e:SetText(L"Blinking unread msgs:")
    W_SETTINGS.LabelFlash = e
    
    -- 'flashtab' Checkbox
    e = W_SETTINGS("Checkbox")
    e:AnchorTo(W_SETTINGS.LabelFlash, "right", "left", 0, 0)
    e.OnLButtonUp =
        function()
             WarWhisperer.Settings.flashtab = W_SETTINGS.CheckFlash:GetValue()
			 if (WarWhisperer.Settings.flashtab) then
				RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "warwhisperer.UpdateFlashing")
			 else
				UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "warwhisperer.UpdateFlashing")
			 end			 
        end		
	e:SetValue(WarWhisperer.Settings.flashtab)
    W_SETTINGS.CheckFlash = e		

    -- Font
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 145)
    e:Align("leftcenter")
    e:SetText(L"Font:")
	e:Font("font_clear_small")
    W_SETTINGS.LabelFont = e
    
    -- edit
    e = W_SETTINGS("Combobox")
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 175)
    for _,font in ipairs( ChatSettings.Fonts) do e:Add(font.shownName) end
	e:SelectIndex(WarWhisperer.Settings.font)
    W_SETTINGS.EditFont = e	
	
    e = W_SETTINGS("Button")
    e:Resize(200)
    e:SetText(L"Apply Font")
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 30, 210)
    e.OnLButtonUp = function() warwhisperer.UpdateWindows() end
    W_SETTINGS.ButtonApply = e	
	
    -- Channels
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 265)
    e:Align("leftcenter")
    e:SetText(L"Channel:")
	e:Font("font_clear_small")
    W_SETTINGS.LabelChannel = e
    
    -- edit
    e = W_SETTINGS("Combobox")
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 290)--
    W_SETTINGS.EditChannel = e	
	
    e = W_SETTINGS("Button")
    e:Resize(200)
    e:SetText(L"Edit Channel")
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 30, 335)
    e.OnLButtonUp = function() warwhisperer.EditChannel() end
    W_SETTINGS.ButtonEditChannel = e		
	
    e = W_SETTINGS("Button")
    e:Resize(200)
    e:SetText(L"Chat Colors")
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 30, 375)
    e.OnLButtonUp = function() 
		if (EA_ChatTabManager.activeTabId == 0) then
			EA_ChatTabManager.activeTabId = 1
		end
		WindowSetShowing( "ChatOptionsWindow", true)
	end
    W_SETTINGS.ButtonEditColors = e		
	
    -- Moveable
    e = W_SETTINGS("Label")
    e:Resize(200)
    e:AnchorTo(W_SETTINGS.SubTitle, "bottomleft", "topleft", 0, 420)
    e:Align("leftcenter")
    e:SetText(L"Moveable:")
	e:Font("font_clear_small")
    W_SETTINGS.LabelMove = e
	
	e = W_SETTINGS("Checkbox")
    e:AnchorTo(W_SETTINGS.LabelMove, "right", "left", 0, 0)
    e.OnLButtonUp =
        function()
            WarWhisperer.Settings.moveable = not WarWhisperer.Settings.moveable
			WindowSetMovable("warwhispererMain", WarWhisperer.Settings.moveable)
        end		
	e:SetValue(WarWhisperer.Settings.moveable)
    W_SETTINGS.CheckMove = e	
	
    -- close button
    e = W_SETTINGS("Closebutton")
	W_SETTINGS.CloseButton = e
    W_SETTINGS.CloseButton.OnLButtonUp =
        function()
            W_SETTINGS:Hide()
        end		
	
	warwhisperer.ReinitChannellist()
	W_SETTINGS:Hide()

	warwhisperer.UpdateSettings()
	warwhisperer.UpdateTabs()  
	warwhisperer.UpdateSpecialIcons()
end

function warwhisperer.EditChannel() 
	EA_Window_ContextMenu.CreateContextMenu("warwhispererEditChannelContextMenu")

	local num = 0
	for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
		num = num + 1
		if (W_SETTINGS.EditChannel:SelectedIndex() == num) then
		
			EA_Window_ContextMenu.AddMenuItem(
						L"---- "..channelobj.title..L" ----", 
						nil, 
						true, 
						true)
									
			if (WarWhisperer.Settings.channelsdisplay[channelobj.name] ~= nil) and WarWhisperer.Settings.channelsdisplay[channelobj.name] == true then
				EA_Window_ContextMenu.AddMenuItem(
							L"Disable this channel", 
							warwhisperer.DisableChannel, 
							false, 
							true)	
			else
				EA_Window_ContextMenu.AddMenuItem(
							L"Enable this channel", 
							warwhisperer.EnableChannel, 
							false, 
							true)
			end
	
			EA_Window_ContextMenu.AddMenuItem(
						L"- Filters displayed:", 
						nil, 
						true, 
						true)	
						
			for filterkey, filter in pairs (channelobj.inputFilter) do
				EA_Window_ContextMenu.AddMenuItem(
							L"* "..(ChatSettings.Channels[filter].name), 
							nil, 
							false, 
							true)	
			end
			
			EA_Window_ContextMenu.AddMenuItem(
						L"- Filter outgoing:", 
						nil, 
						true, 
						true)	
						
			EA_Window_ContextMenu.AddMenuItem(
							L"* "..(ChatSettings.Channels[channelobj.outputFilter].name), 
							nil, 
							false, 
							true)							
						
			EA_Window_ContextMenu.Finalize()
			return true
		end
	end	
end

function warwhisperer.ReinitChannellist()
		num = W_SETTINGS.EditChannel:SelectedIndex()
		if (num == nil) or (num < 1) then
			num = 1
		end
		W_SETTINGS.EditChannel:Clear()
		for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do 
			if (WarWhisperer.Settings.channelsdisplay[channelobj.name] ~= nil) and WarWhisperer.Settings.channelsdisplay[channelobj.name] == true then
				W_SETTINGS.EditChannel:Add(channelobj.title..L" - enabled") 
			else
				W_SETTINGS.EditChannel:Add(channelobj.title..L" - disabled") 
			end
		end
		W_SETTINGS.EditChannel:SelectIndex(num)
end

function warwhisperer.EnableChannel()
	local num = 0
	for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
		num = num + 1
		if (W_SETTINGS.EditChannel:SelectedIndex() == num) then
			WarWhisperer.Settings.channelsdisplay[channelobj.name] = true
			warwhisperer.ReinitChannellist()
			return true
		end
	end
end

function warwhisperer.DisableChannel()
	local num = 0
	for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
		num = num + 1
		if (W_SETTINGS.EditChannel:SelectedIndex() == num) then
			WarWhisperer.Settings.channelsdisplay[channelobj.name] = false
			warwhisperer.ReinitChannellist()
			return false
		end
	end
end


function warwhisperer.OnMouseOverWarBoard()
	local nall=table.getn(warwhisperer.conversations)	

	Tooltips.CreateTextOnlyTooltip( "WarBoard_WarWhisperer", nil)
	Tooltips.AnchorTooltip( WarBoard.GetModToolTipAnchor( "WarBoard_WarWhisperer" ) )
	Tooltips.SetTooltipColor( 1, 1, 255, 255, 255 )
	Tooltips.SetTooltipText( 1, 1, L"WarWhisperer" )
	Tooltips.SetTooltipText( 2, 1, L"Total Conversations: " ) 
	Tooltips.SetTooltipText( 2, 2, towstring( nall ) )
	local i = 4
	for subkey,obj in ipairs(warwhisperer.conversations) do
		Tooltips.SetTooltipText( i, 1, obj.name )
		if (obj.marked == 1) then
			Tooltips.SetTooltipColor( i, 1, 255, 0,0 )
			Tooltips.SetTooltipText( i, 2, L"*" )	
		else
			Tooltips.SetTooltipColor( i, 1, 222, 255,255 )
		end	

		i = i + 1
	end
	Tooltips.Finalize()
end

function warwhisperer.SendBoxOnKeyEscape()
	TextEditBoxSetText("warwhispererMainSendBox", L"")
end

function warwhisperer.SendBoxOnKeyEnter()
	warwhisperer.SendText(TextEditBoxGetText("warwhispererMainSendBox"))
	TextEditBoxSetText("warwhispererMainSendBox", L"")
	d("WindowAssignFocus2")
	WindowAssignFocus("warwhispererMainInner",true)	
end

function warwhisperer.Hide()
	WindowSetShowing("warwhispererMain", false)
	warwhisperer.UpdateWindows()
end
    
function warwhisperer.Toggle()	
	if (WindowGetShowing("warwhispererMain")) then
		WindowSetShowing("warwhispererMain", false)
		warwhisperer.UpdateWindows()
	else
		WindowSetShowing("warwhispererMain", true)
		warwhisperer.UpdateWindows()
	end
end
	
function warwhisperer.ScrollTabLeft()
	local tabname = "warwhispererMainTab1"
	if (DoesWindowExist(tabname)) then
		local scrollOffset = WindowGetOffsetFromParent(tabname)
		scrollOffset = scrollOffset + 50 
		if (scrollOffset > 4) then
			scrollOffset = 4
		end
		WindowSetOffsetFromParent(tabname, scrollOffset, 4 )
	end
	warwhisperer.CheckTabOffset()
end

function warwhisperer.ScrollTabRight()
	local nall=table.getn(warwhisperer.conversations)	
	local tabname = "warwhispererMainTab1"
	if (DoesWindowExist(tabname)) then
		local scrollOffset = WindowGetOffsetFromParent(tabname)
		local maxwindows = warwhisperer.GetMaxTabCount()
		scrollOffset = scrollOffset - 50 
		if (nall > maxwindows) then
			if (scrollOffset < -((nall-maxwindows)*100)+44) then
				scrollOffset = -((nall-maxwindows)*100)+44
			end
		else
			scrollOffset = 4
		end
		WindowSetOffsetFromParent(tabname, scrollOffset, 4 )
	end
	warwhisperer.CheckTabOffset()
end

function warwhisperer.GetMaxTabCount()
		local mainx,mainy = WindowGetDimensions( "warwhispererMainTabContainer" )
		local maxcount = mainx
		maxcount = math.floor(maxcount / 50)
		return maxcount
end

function warwhisperer.CheckTabOffset()
	local nall=table.getn(warwhisperer.conversations)
	local tabname = "warwhispererMainTab1"
	ButtonSetDisabledFlag("warwhispererMainTabLeft", false)
	ButtonSetDisabledFlag("warwhispererMainTabRight", false)	

	if (DoesWindowExist(tabname)) then	
		scrollOffset = WindowGetOffsetFromParent(tabname)
		local maxwindows = warwhisperer.GetMaxTabCount()
		if (nall > maxwindows) then
			if (scrollOffset < -((nall-maxwindows)*100)+44) then
				scrollOffset = -((nall-maxwindows)*100)+44
			end
		else
			scrollOffset = 4
		end
		WindowSetOffsetFromParent(tabname, scrollOffset, 4 )
		
		if (scrollOffset == 4) then
			ButtonSetDisabledFlag("warwhispererMainTabLeft", true)
		end
		if ( scrollOffset == -((nall-maxwindows)*100)+44) then
			ButtonSetDisabledFlag("warwhispererMainTabRight", true)
		end			
		
		if (nall < maxwindows+1) then
			ButtonSetDisabledFlag("warwhispererMainTabRight", true)
		end					
	else
		ButtonSetDisabledFlag("warwhispererMainTabLeft", true)
		ButtonSetDisabledFlag("warwhispererMainTabRight", true)	
	end
end
  
  
function warwhisperer.UpdateTabs()  
	local nall=table.getn(warwhisperer.conversations)
	
	for i=nall+1, 100, 1 do
		if (DoesWindowExist("warwhispererMainTab"..i)) then
			DestroyWindow("warwhispererMainTab"..i)
		end
	end

	WARWHISPERER_ELEMENT = {}
	
	local n = 0
	for _,obj in ipairs(warwhisperer.conversations) do
		local name = obj.name
				
		local addtab = true
		
		if (addtab) then
			n = n + 1
			if (not DoesWindowExist("warwhispererMainTab"..n)) then
			    if CreateWindowFromTemplate("warwhispererMainTab"..n, "EA_Button_BottomTab", "warwhispererMainTabContainerScrollChild") then
					WindowSetDimensions("warwhispererMainTab"..n, 100, 50)
					WindowClearAnchors("warwhispererMainTab"..n)

					if (n == 1) then
						WindowAddAnchor("warwhispererMainTab"..n, "topleft", "warwhispererMainTabContainerScrollChild", "topleft", 2, 2)
					else
						WindowAddAnchor("warwhispererMainTab"..n, "topright", "warwhispererMainTab"..n-1, "topleft", 0, 0)
					end
					WindowRegisterCoreEventHandler("warwhispererMainTab"..n, "OnLButtonUp", "WARWHISPERER_ELEMENT.events.OnLButtonUp.warwhispererMainTab"..n)
					WindowRegisterCoreEventHandler("warwhispererMainTab"..n, "OnRButtonUp", "WARWHISPERER_ELEMENT.events.OnRButtonUp.warwhispererMainTab"..n)
				end
			end
			
			local function eventFuncInner(pressedButton)
				return {
					__index = function(self,index)
								self[index]=function(...)
									warwhisperer.TabPress(pressedButton,index)
								end
								return self[index]
							end
				}
			end
			
			local eventFunc={
				__index = function(self,index)
					 self[index]=setmetatable({}, eventFuncInner(index))
					return self[index]
				  end
			}
			
			WARWHISPERER_ELEMENT.events = setmetatable({}, eventFunc)			
			
			ButtonSetText("warwhispererMainTab"..n, name)
			
			if (name == warwhisperer.active) then
				ButtonSetPressedFlag( "warwhispererMainTab"..n, true )
			else
				ButtonSetPressedFlag( "warwhispererMainTab"..n, false )
			end
			
			if (obj.marked == 1) then
				ButtonSetTextColor("warwhispererMainTab"..n, 0, 255, 0,0)
			else
				ButtonSetTextColor("warwhispererMainTab"..n, 0, 222, 192,50)
			end					
		end	
	end
	
	if (WarBoard ~= nil) and DoesWindowExist("WarBoard_WarWhisperer") then
		LabelSetTextColor("WarBoard_WarWhispererText", 255, 255, 255)
		for _,obj in ipairs(warwhisperer.conversations) do
			if (obj.marked == 1) then
				LabelSetTextColor("WarBoard_WarWhispererText", 255, 0, 0)
				break
			end
		end
	end
	
	warwhisperer.UpdateWindows()	
	warwhisperer.CheckTabOffset()
end

function warwhisperer.TabPress(TabButton,TabObject)
	local user = ButtonGetText(TabObject)
	if (TabButton == "OnLButtonUp") then
		warwhisperer.ToggleChat(user)
	end
	if (TabButton == "OnRButtonUp") then
		warwhisperer.RemoveConversation(user)
	end	
end

function warwhisperer.UpdateSpecialIcons()
	ButtonSetDisabledFlag("warwhispererMainInnerTextCareerButton",false)
	ButtonSetDisabledFlag("warwhispererMainInnerTextGroupInviteButton",true)
	ButtonSetDisabledFlag("warwhispererMainInnerTextAddFriendButton",true)
	ButtonSetDisabledFlag("warwhispererMainInnerTextAddIgnoreButton",true)
	ButtonSetDisabledFlag("warwhispererMainInnerTextTargetButton",true)
	
	WindowSetTintColor("warwhispererMainInnerTextCareerButton",255,255,255)
	WindowSetTintColor("warwhispererMainInnerTextGroupInviteButton",125,125,125)
	WindowSetTintColor("warwhispererMainInnerTextAddFriendButton",125,125,125)
	WindowSetTintColor("warwhispererMainInnerTextAddIgnoreButton",125,125,125)
	WindowSetTintColor("warwhispererMainInnerTextTargetButton",125,125,125)
	
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
			ButtonSetDisabledFlag("warwhispererMainInnerTextCareerButton",false)
			ButtonSetDisabledFlag("warwhispererMainInnerTextGroupInviteButton",false)
			ButtonSetDisabledFlag("warwhispererMainInnerTextAddFriendButton",false)
			ButtonSetDisabledFlag("warwhispererMainInnerTextAddIgnoreButton",false)
			ButtonSetDisabledFlag("warwhispererMainInnerTextTargetButton",false)
			
			WindowSetTintColor("warwhispererMainInnerTextCareerButton",255,255,255)
			WindowSetTintColor("warwhispererMainInnerTextGroupInviteButton",255,255,255)
			WindowSetTintColor("warwhispererMainInnerTextAddFriendButton",255,255,255)
			WindowSetTintColor("warwhispererMainInnerTextAddIgnoreButton",255,255,255)			
			WindowSetTintColor("warwhispererMainInnerTextTargetButton",255,255,255)			
		end
	end
	
	if (warwhisperer.active == L"") then
		local texture, x, y = GetIconData( 2 )
		DynamicImageSetTexture( "warwhispererMainInnerTextCareerButtonIconBase", texture, x, y )		
		DynamicImageSetTextureScale("warwhispererMainInnerTextCareerButtonIconBase", 0.7)
	end
end

--[[
function warwhisperer.InviteUserToGroup()
	if (user == nil) then
		for subkey,obj in ipairs(warwhisperer.conversations) do
			if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
				DialogManager.MakeTwoButtonDialog( warwhisperer.active..L"\n"..GetStringFromTable("SocialStrings", StringTables.Social.TEXT_SOCIAL_INVITETO_PARTY), GetString( StringTables.Default.LABEL_YES ), warwhisperer.RealInviteUserToGroup, GetString( StringTables.Default.LABEL_NO ), nil )
				break
			end
		end
	else

	end
end
]]--

function warwhisperer.PurgeHistory()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) then
			DialogManager.MakeTwoButtonDialog( L"Purge history for "..warwhisperer.active..L"?", GetString( StringTables.Default.LABEL_YES ), warwhisperer.RealPurgeHistory, GetString( StringTables.Default.LABEL_NO ), nil )
			break
		end
	end
end

function warwhisperer.RealPurgeHistory()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) then
			if (obj.conv_type == 1) then
				name = "WarWhisperer"..WStringToString(obj.name)
				user = obj.name
			end
			
			if (obj.conv_type == 99) then
				   for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
						if (L"<"..channelobj.title..L">" == obj.name) then
							name = "InternalWarWhisperer"..channelobj.name
							user = L"Channel_"..channelobj.title
						end
					end
			end
			
			TextLogSetEnabled(name, false )
			TextLogSetIncrementalSaving( name, false, StringToWString("logs/warwhisperer_"..WStringToString(user)..".log"))
			TextLogClear(name)		
			TextLogSetIncrementalSaving( name, true, StringToWString("logs/warwhisperer_"..WStringToString(user)..".log"))
			TextLogSetEnabled(name, true )		

            --Fix for CTD
            TextLogAddEntry(name, 80000, L"")
		end
	end
end

function warwhisperer.InviteUserToGroup()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
		    --SystemData.UserInput.ChatText = L"/invite "..obj.name
		    --BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
			SendChatText(L"/invite "..obj.name, L"")
			break
		end
	end
end

function warwhisperer.AddToFriend()

	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
			local serverfriendslist = GetFriendsList()
			local isfriend = false
			for key,friend in ipairs(serverfriendslist) do
				local user = WStringToString(friend.name)
				user = string.sub(user,0,string.len(user)-2) --some strange chars after the name
				user = StringToWString(user)			
				if (user == obj.name) then
					isfriend = true
				end
			end
			if (not isfriend) then
				--SystemData.UserInput.ChatText = L"/friend "..obj.name
				--BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )				
				SendChatText(L"/friend "..obj.name, L"")
			end
		end
	end
end

function warwhisperer.AddToIgnore()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
			local IgnoreListData = GetIgnoreList()
			local isignored = false
			for key,ignoreuser in ipairs(IgnoreListData) do
				local user = WStringToString(ignoreuser.name)
				user = string.sub(user,0,string.len(user)-2) --some strange chars after the name
				user = StringToWString(user)			
				if (user == obj.name) then
					isignored = true
				end
			end
			if (not isignored) then
				--SystemData.UserInput.ChatText = L"/ignore "..obj.name
				--BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )				
				SendChatText(L"/ignore "..obj.name, L"")
			end			
		end
	end
end

function warwhisperer.TargetPlayer()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
		    --SystemData.UserInput.ChatText = L"/target "..obj.name
		    --BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
			SendChatText(L"/target "..obj.name, L"")
			break
		end
	end
end


function warwhisperer.MouseOverCareerMouseover()
	for subkey,obj in ipairs(warwhisperer.conversations) do
		if (obj.name == warwhisperer.active) and (obj.conv_type == 1) then
			local text = obj.name
			if (obj.rank ~= 0) then
				text = text..L" - "..obj.rank..L" "..obj.career
			end
			if (obj.guild ~= L"") then
				text = text..L" <"..obj.guild..L">"
			end
			Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,text)
			Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
		end
	end
	
	warwhisperer.MouseOverSpecialButtons()
end

function warwhisperer.MouseOverSpecialButtons()
	WindowSetHandleInput("warwhispererMainInnerTextScrollbar", false)
	if (SystemData.MouseOverWindow.name == "warwhispererMainInnerTextGroupInviteButton") then
		Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,GetStringFromTable("SocialStrings", StringTables.Social.TEXT_SOCIAL_INVITETO_PARTY))
		Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
	end
	if (SystemData.MouseOverWindow.name == "warwhispererMainInnerTextAddFriendButton") then
		Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,GetStringFromTable("SocialStrings", StringTables.Social.LABEL_SOCIAL_FRIENDS_BUTTON_ADDFRIEND))
		Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)		
	end
	if (SystemData.MouseOverWindow.name == "warwhispererMainInnerTextAddIgnoreButton") then
		Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,GetStringFromTable("SocialStrings", StringTables.Social.LABEL_SOCIAL_IGNORE_BUTTON_ADDIGNORE))
		Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
	end
	if (SystemData.MouseOverWindow.name == "warwhispererMainInnerTextTargetButton") then
		Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,GetStringFromTable("Default", StringTables.Default.LABEL_TARGET))
		Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
	end	
	--TextLogAddEntry("Chat", 0, L"WarWhisperer DEBUG: dis")
end

function warwhisperer.MouseOverSpecialButtonsEnd()
	WindowSetHandleInput("warwhispererMainInnerTextScrollbar", true)
	--TextLogAddEntry("Chat", 0, L"WarWhisperer DEBUG: en")
end


function warwhisperer.MarkTabForUser(user)
	if (user ~= warwhisperer.active) then
		for key,obj in ipairs(warwhisperer.conversations) do
			if (user == obj.name) then
				warwhisperer.conversations[key].marked = 1
				if (WarBoard ~= nil) and DoesWindowExist("WarBoard_WarWhisperer") then
					LabelSetTextColor("WarBoard_WarWhispererText", 255, 0, 0)
				end
				break
			end			
		end	
	end
	warwhisperer.UpdateTabs()  
end

function warwhisperer.shortName(strString)
    --used from wscd
    strString = wstring.match(wstring.gsub(wstring.gsub(wstring.gsub(strString,L" of ", L"O"), L"%s",L""), L"%l*", L""), L"([^^]+)^?.*")
    return strString
end






function warwhisperer.Test()
--WarWhisperer.Settings.channelsdisplay
--[[
local link  = CreateHyperLink( L"WW:VER:0.10", L"", {}, {} )	
local text = link .. L"WW - TEST";
SystemData.UserInput.ChatChannel = L"/4"
SystemData.UserInput.ChatText = text
BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)	

d("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do

	if WarWhisperer.Settings.channelsdisplay[channelobj.name] ~= nil then
		d(channelobj.name)
		d(WarWhisperer.Settings.channelsdisplay[channelobj.name])
	end
end
	TextLogAddEntry("Chat", 0, L"WarWhisperer DEBUG"..test_num);
	
	d("Dump All")
	d(warwhisperer.conversations)
	d("Dump Last")
	d(warwhisperer.lastconversations)
	
	test_num = test_num + 1
	local texture, x, y = GetIconData( test_num )
	DynamicImageSetTexture( "warwhispererMainInnerTextCareerButtonIconBase", texture, x, y )	
	
	--warwhisperer.AddConversation(L"aa",1) 
	--warwhisperer.AddConversation(L"ab",1) 
	--warwhisperer.AddConversation(L"ac",1) 
	--warwhisperer.AddConversation(L"ad",1) 	
	--warwhisperer.AddConversation(L"ae",1) 
	local username1 = L"Sampleuser"
	local username2 = L"Yourname"
	warwhisperer.AddConversation(username1,1) 
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"Hey")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"Hi")  --otheruser
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"How are you?")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"fine")  --otheruser
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"cool i'm too")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"great")  --otheruser
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"some testchat to test")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"for you")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"i know i speaking with myself")  --otheruser
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"yea :D")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"ok we reached alsmost the end")  --otheruser
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username2)..L"ok, bye")  --you
	TextLogAddEntry("WarWhisperer"..WStringToString(username1), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(username1)..L"bye dude happy testing.")  --otheruser	
	]]--
end

function warwhisperer.CleanLogDisplayAddLog(logdisplay,logname,somebool)
	table.insert(warwhisperer.logdisplay,logname)
	LogDisplayAddLog(logdisplay,logname,somebool)
end

function warwhisperer.ClearOldChat()
	for key,logname in pairs(warwhisperer.logdisplay) do
		LogDisplayRemoveLog("warwhispererMainInnerText", logname) 
	end

	warwhisperer.logdisplay = {}
	
    --[[
	if (warwhisperer.active ~= L"") then
		LogDisplayRemoveLog("warwhispererMainInnerText", "WarWhisperer"..WStringToString(warwhisperer.active)) 	
	end
	
	for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
		LogDisplayRemoveLog("warwhispererMainInnerText", "InternalWarWhisperer"..channelobj.name) 
	end
	]]--
end

function warwhisperer.ToggleChat(user)
	
	warwhisperer.ClearOldChat()
	
	for key,obj in ipairs(warwhisperer.conversations) do
		if (user == obj.name) then	
	
			if (obj.conv_type == 1) then
				warwhisperer.CleanLogDisplayAddLog("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), true) 	
				local color = ChatSettings.ChannelColors[  SystemData.ChatLogFilters.TELL_RECEIVE ]
			    LogDisplaySetFilterColor("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), 1, color.r, color.g, color.b ) -- Magenta for received
				local color = ChatSettings.ChannelColors[  SystemData.ChatLogFilters.TELL_SEND ]
			    LogDisplaySetFilterColor("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), 2, color.r, color.g, color.b ) -- Dark Magenta for send		
				LogDisplaySetFilterColor("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), 3,128, 128, 128 ) -- grey for errors	
				LogDisplaySetFilterColor("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), 4,255, 255, 0 ) -- csr color	
				LogDisplaySetFilterColor("warwhispererMainInnerText", "WarWhisperer"..WStringToString(user), 80000,180, 180, 180 ) -- grey for history	
			end
			
			if (obj.conv_type == 99) then
				   for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
						if (L"<"..channelobj.title..L">" == user) then
							warwhisperer.CleanLogDisplayAddLog("warwhispererMainInnerText", "InternalWarWhisperer"..channelobj.name, true)
							LogDisplaySetFilterColor("warwhispererMainInnerText", "InternalWarWhisperer"..channelobj.name, 80000,180, 180, 180 ) -- grey for history						
							for filterkey, filter in pairs(channelobj.inputFilter) do
								local color = ChatSettings.ChannelColors[ filter ]
								LogDisplaySetFilterColor( "warwhispererMainInnerText", "InternalWarWhisperer"..channelobj.name, filter, color.r, color.g, color.b )	
							end
						end
					end			
			end
			
			warwhisperer.active = user
			warwhisperer.conversations[key].marked = 0
			
			--CareerIcon
			if (obj.careerid ~= 0) then
				local texture, x, y = GetIconData( Icons.GetCareerIconIDFromCareerLine( obj.careerid ) )
				DynamicImageSetTexture( "warwhispererMainInnerTextCareerButtonIconBase", texture, x, y )
				DynamicImageSetTextureScale("warwhispererMainInnerTextCareerButtonIconBase", 0.7)
			else
				local texture, x, y = GetIconData( 2 )
				DynamicImageSetTexture( "warwhispererMainInnerTextCareerButtonIconBase", texture, x, y )		
				DynamicImageSetTextureScale("warwhispererMainInnerTextCareerButtonIconBase", 0.7)				
			end
			break
		end
	end	
	
	warwhisperer.UpdateTabs()  
	warwhisperer.UpdateSpecialIcons()
end


function warwhisperer.AddLastConversation(user) 
	for key,obj in ipairs(warwhisperer.lastconversations) do
		if(obj.name==user) then
			warwhisperer.lastconversations[key].last = false
			return
		end
	end
	
	local newobj = {}
	newobj.name = user	
	newobj.last = false

	
	table.insert(warwhisperer.lastconversations,newobj)	
end

function warwhisperer.LastConversationState(user,state) 
	for key,obj in ipairs(warwhisperer.lastconversations) do
		if(obj.name==user) then
			if (state ~= nil) then
				warwhisperer.lastconversations[key].last = state
			end
			return warwhisperer.lastconversations[key].last
		end
	end
	
	return nil
end

function warwhisperer.InitTextLog(name,num,user,conv_type)
	TextLogCreate(name,num)
	
	local i = TextLogGetNumEntries(name)
	if (i == 0) then
	
		TextLogClear("InternalWarWhispererHistoryHelper")
		TextLogLoadFromFile("InternalWarWhispererHistoryHelper", StringToWString("logs/warwhisperer_"..WStringToString(user)..".log"))
		
		TextLogSetIncrementalSaving( name, true, StringToWString("logs/warwhisperer_"..WStringToString(user)..".log"))
		TextLogSetEnabled( name, true )	
		
		for i=0,  TextLogGetNumEntries("InternalWarWhispererHistoryHelper") -1 , 1 do
			local timestamp, id, text = TextLogGetEntry("InternalWarWhispererHistoryHelper", i)
			text = wstring.sub(text,22,-2) -- remove date and linebreak
			TextLogAddEntry(name, 80000, warwhisperer.textfilter(text))		
		end	
		
		-- fix for a ctd. war crashes if you load a empty (only a \r\n) via TextLogLoadFromFile
		local i = TextLogGetNumEntries(name)
		if (i == 0) then
			TextLogAddEntry(name, 80000, L"")
		end
	end
end

function warwhisperer.AddConversation(user,conv_type,specialchannel) 
	--default is whisper is 1
	
	if (conv_type == nil) then
		conv_type = 1
	end
	
	
	for key,obj in ipairs(warwhisperer.conversations) do
		if(obj.name==user) then
			return
		end
	end

	local newobj = {}
	newobj.name = user	
	newobj.marked = 0
	newobj.conv_type = conv_type
	newobj.career = L""	
	newobj.careerid = 0
	newobj.guild = L""		
	newobj.rank = 0	
	
	table.insert(warwhisperer.conversations,newobj)
	
	if (conv_type == 1) then
		
	
		warwhisperer.InitTextLog("WarWhisperer"..WStringToString(user),5000,user,conv_type)		

		if (WarWhisperer.Settings.queryuser) then
			warwhisperer.GetPlayerInfo(user)
		end
		warwhisperer.AddLastConversation(user) 
	end
	
	if (conv_type == 99) then
		warwhisperer.InitTextLog("InternalWarWhisperer"..specialchannel.name,specialchannel.id,L"Channel_"..specialchannel.title,99)		
	end

	if (warwhisperer.active == L"") then
		for key,obj in ipairs(warwhisperer.conversations) do
			warwhisperer.ToggleChat(obj.name)
			break
		end	
	end
	
	warwhisperer.UpdateTabs()  
end

function warwhisperer.RemoveConversation(user,force)
	for key,obj in ipairs(warwhisperer.conversations) do
		if(obj.name==user) then
			if (obj.conv_type == 1) then
				warwhisperer.LastConversationState(user,true)
			end
			table.remove(warwhisperer.conversations,key)
		end		
	end
		
	local n=table.getn(warwhisperer.conversations)
	
	if (n == 0) then
		--WindowSetShowing("warwhispererMain",false)
		warwhisperer.ClearOldChat()
		warwhisperer.active = L""
		warwhisperer.UpdateSpecialIcons()
	end
	
	if (warwhisperer.active == user) then
		for key,obj in ipairs(warwhisperer.conversations) do
			warwhisperer.ToggleChat(obj.name)
			break
		end	
		
	else
		warwhisperer.UpdateTabs()  
	end
end
	
function warwhisperer.SendText(text)

	if (text == L"") then
		return
	end

	
	for key,obj in ipairs(warwhisperer.conversations) do
		if (warwhisperer.active == obj.name) then
			if (obj.conv_type == 1) then
				SystemData.UserInput.ChatChannel = ChatSettings.Channels[ SystemData.ChatLogFilters.TELL_SEND ].serverCmd .. L" "..warwhisperer.active
			end
			
			if (obj.conv_type == 99) then
				   for channelkey,channelobj in pairs(warwhisperer_defaultchannels) do
						if (L"<"..channelobj.title..L">" == obj.name) then
							SystemData.UserInput.ChatChannel = ChatSettings.Channels[ channelobj.outputFilter ].serverCmd
						end
				  end
		   end
	
			--SystemData.UserInput.ChatText = text
			--BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)	
			SendChatText(L"/w "..warwhisperer.active..L" "..text, L"")
			break
		end
	end		
end	

function warwhisperer.OnPressSearchPlayerButton()
	WindowUnregisterEventHandler( "SocialWindowTabSearch", SystemData.Events.SOCIAL_SEARCH_UPDATED)	
	WindowRegisterEventHandler( "SocialWindowTabSearch", SystemData.Events.SOCIAL_SEARCH_UPDATED, "SocialWindowTabSearch.OnSearchUpdated")	
	warwhisperer.oldOnPressSearchPlayerButton()
end

function warwhisperer.EA_ChatWindowInsertText( text )
	--d("chat insert hooked")
	--d(text)
	if( WindowGetShowing( "EA_TextEntryGroupEntryBox" ) == false ) then
		if( WindowGetShowing( "warwhispererMain" )) then
			if (warwhisperer.active ~= L"") then
				TextEditBoxInsertText( "warwhispererMainSendBox", text )
				d("WindowAssignFocus3")
                WindowAssignFocus("warwhispererMainSendBox",true)
				return
			end
		end		
	end
	warwhisperer.oldEA_ChatWindowInsertText(text)
end

function warwhisperer.EA_ChatWindowSwitchChannelWithExistingText(existingText)
	--d("chat channel hooked")
	--d(existingText)
	local test = WStringToString(existingText)
	if (string.sub(test,0,5) == "/tell") then
		local suser = string.sub(test,6,string.len(test))
		suser = string.gsub(suser, "^%s*(.-)%s*$", "%1")
		local user = StringToWString(suser)
		--sometimes the name contains strange chars at its end.
		if (wstring.sub(user,wstring.len(user)-1,wstring.len(user)-1) == L"^") then
			user = wstring.sub(user,0,wstring.len(user)-2)
		end		
		if ( user == nil or WStringToString(user) == '') then 
				d(L"There is no user")
	            return
		end 
		warwhisperer.AddConversation(user)
		warwhisperer.ShowChat(user, true)
		d("WindowAssignFocus4")
		WindowAssignFocus("warwhispererMainSendBox",true)
		return
	end
	warwhisperer.oldEA_ChatWindowSwitchChannelWithExistingText(existingText)
end

function warwhisperer.SetHook()
	 if not firstLoad then return end
	 firstLoad = false
	 warwhisperer.oldOnPressSearchPlayerButton = SocialWindowTabSearch.OnPressSearchPlayerButton
	 SocialWindowTabSearch.OnPressSearchPlayerButton = warwhisperer.OnPressSearchPlayerButton
	 warwhisperer.oldEA_ChatWindowInsertText = EA_ChatWindow.InsertText
	 EA_ChatWindow.InsertText = warwhisperer.EA_ChatWindowInsertText
	 
	 warwhisperer.oldEA_ChatWindowSwitchChannelWithExistingText = EA_ChatWindow.SwitchChannelWithExistingText
	 EA_ChatWindow.SwitchChannelWithExistingText = warwhisperer.EA_ChatWindowSwitchChannelWithExistingText 
end

function warwhisperer.GetPlayerInfo(user)
    if ( user == nil or WStringToString(user) == '') then 
				d(L"There is no user")
	            return
	end 
    -- first try friendslist for charinfos
	local serverfriendslist = GetFriendsList()
	for key,friend in ipairs(serverfriendslist) do
		local luser = WStringToString(friend.name)
		luser = string.sub(luser,0,string.len(luser)-2) --some strange chars after the name
		luser = StringToWString(luser)			
		if (user == luser) then
			for subkey,obj in ipairs(warwhisperer.conversations) do
				if (user == obj.name) then
					obj.career = friend.careerName
					obj.careerid = warwhisperer.getCareerLineIdbyName(obj.career)
					obj.guild = friend.guildName
					if ( friend.zoneID ~= 0 ) then
						obj.rank = friend.rank
					else
						obj.rank = 0
					end
					warwhisperer.conversations[subkey] = obj
					return
				end
			end
		end
	end 
	
	-- try guild data next
	if (GameData.Guild.m_GuildName ~= L"") then
		local rosterData = GetGuildMemberData()
		if( rosterData ~= nil ) then
			for key,guildie in ipairs(rosterData) do
				local luser = WStringToString(guildie.name)
				luser = string.sub(luser,0,string.len(luser)-2) --some strange chars after the name
				luser = StringToWString(luser)			
				if (user == luser) then
					for subkey,obj in ipairs(warwhisperer.conversations) do
						if (user == obj.name) then
							obj.career = guildie.careerString
							obj.careerid = warwhisperer.getCareerLineIdbyName(obj.career)
							obj.guild = GameData.Guild.m_GuildName
							obj.rank = guildie.rank
							warwhisperer.conversations[subkey] = obj
							return
						end
					end
				end
			end 
		end
	end
	
	-- try scenario data, ok, guild is missing but who cares :D
    local interimPlayerData = GameData.GetScenarioPlayers()
    if( interimPlayerData ~= nil ) then
		for key,scuser in ipairs(interimPlayerData) do
			local luser = WStringToString(scuser.name)
			luser = string.sub(luser,0,string.len(luser)-2) --some strange chars after the name
			luser = StringToWString(luser)			
			if (user == luser) then
				for subkey,obj in ipairs(warwhisperer.conversations) do
					if (user == obj.name) then
						obj.career = scuser.career
						obj.careerid = warwhisperer.getCareerLineIdbyName(obj.career)
						obj.rank = scuser.rank
						warwhisperer.conversations[subkey] = obj
						return
					end
				end
			end
		end
	end	

	--Send searchrequest for user if no other method found to get charinfos
	--disable search in socialwindow
	WindowUnregisterEventHandler( "SocialWindowTabSearch", SystemData.Events.SOCIAL_SEARCH_UPDATED)		
	SendPlayerSearchRequest(user, L"", L"", {-1}, 1,40, false)
end

function warwhisperer.getCareerLineIdbyName(career)
	local cid = 0
	for i=1, 24, 1 do
		local testnameMale =  GetStringFromTable("CareerLinesMale", i)
		local testnameFemale =  GetStringFromTable("CareerLinesFemale", i)
		if (testnameMale == career) or (testnameFemale == career) then
			cid = i
			break
		end
	end
	return cid
end

function warwhisperer.getPlayerInfoUpdate()
	local SearchListData = GetSearchList()
    if ( SearchListData ~= nil ) then
        for key, value in ipairs( SearchListData ) do
			local user = WStringToString(value.name)
			user = string.sub(user,0,string.len(user)-2) --some strange chars after the name
			user = StringToWString(user)
			for subkey,obj in ipairs(warwhisperer.conversations) do
				if(obj.name == user) then
					obj.career = value.career
					obj.careerid = warwhisperer.getCareerLineIdbyName(obj.career)
					obj.guild = value.guildName
					if ( value.zoneID ~= 0 ) then
						obj.rank = value.rank
					else
						obj.rank = 0
					end
					warwhisperer.conversations[subkey] = obj
				end
			end
		end
	end
	
	if (warwhisperer.active ~= L"") then
		warwhisperer.ToggleChat(warwhisperer.active)  
	end
end

function warwhisperer.ShowChat(user, force)
    
	if (user ~= nil) then
		warwhisperer.ToggleChat(user)
	else
	   return
	end

	if ((WarWhisperer.Settings.nocombatpopup == true) and (GameData.Player.inCombat == true) and force ~= true) then

		return
	end
	
    --WindowSetDrawWhenInterfaceHidden( "warwhispererMain", GameData.SiegeWeapon.isPlayerController )
	WindowSetShowing("warwhispererMain",true)
end

function warwhisperer.GotReply()
	if (SystemData.UserInput.ReplyToPlayerName[1] ~= L"") then
		if warwhisperer.debug then 
		d("Got a replay from:"..SystemData.UserInput.ReplyToPlayerName[1])
		end
		warwhisperer.AddConversation(SystemData.UserInput.ReplyToPlayerName[1])
		warwhisperer.ShowChat(SystemData.UserInput.ReplyToPlayerName[1], true)

		--hide default chatwindow reply thingy
	    WindowAssignFocus( "EA_TextEntryGroupEntryBoxTextInput", false )
	    WindowSetShowing( "EA_TextEntryGroupEntryBox", false )
		
		-- show ours
		d("WindowAssignFocus5")
		WindowAssignFocus("warwhispererMainSendBox",true)	
	else
	   if warwhisperer.debug then 
		   d("Got a replay from no name")
		end
	end
end

function warwhisperer.GetPlayerLink(name,additionalname)
    local data  = L"PLAYER:"..name
    local text  = name   
	if (additionalname ~= nil) then
		text  = additionalname..L"|"..name 
	end
    
   local link  = CreateHyperLink( data, text, {}, {} )
   
   link = L"["..link..L"] "
   
   return link
end

function warwhisperer.GetTimeStamp() 
	TextLogClear("InternalWarWhispererTimeStampHelper")
	TextLogAddEntry("InternalWarWhispererTimeStampHelper", 0, L"")  
	local lastTime = TextLogGetEntry("InternalWarWhispererTimeStampHelper", 0)
	local link  = CreateHyperLink( L"WARWHISPERER:TIME", lastTime, {128,128,128}, {} )	
	link = link..L" "
	return link
end

function warwhisperer.dumpMessage(typemessage)
	  if warwhisperer.debug then 
		d("Start message dump")
		d("Type:"..typemessage.type)
		d("Text:"..tostring(typemessage.text))
		if GameData.ChatData.name ~= nil then
			d("From:"..tostring(typemessage.name))
        else
		    d("From:No name in the message")
		end
	   	-- d(wstring.sub(GameData.ChatData.text,2,wstring.len(GameData.ChatData.text)))
	  d("End message dump")
	  end
end



function warwhisperer.GotRX()
       warwhisperer.dumpMessage(GameData.ChatData)
	   -- ERROR MAPPING
	   if( GameData.ChatData.type == 26 ) then
		local user = nil
		
		--- localized error mapping to correct tab
		
		if SystemData.Settings.Language.active == SystemData.Settings.Language.ENGLISH then
			local pos = string.find(WStringToString(GameData.ChatData.text), " is not in the game,")
			if (pos) then
				user = string.sub(WStringToString(GameData.ChatData.text), 0, pos-1)
			end		
		end
		
		if SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN then
			local pos = string.find(WStringToString(GameData.ChatData.text), " nicht im Spiel")
			local pos2 = string.find(WStringToString(GameData.ChatData.text), "et sich")
			if (pos) then
				if (pos2) then
					user = string.sub(WStringToString(GameData.ChatData.text), pos2+8, pos-1)
				end
			end			
		end
		
		if SystemData.Settings.Language.active == SystemData.Settings.Language.SPANISH then
			local pos = string.find(WStringToString(GameData.ChatData.text), " no est� jugando")
			if (pos) then
				user = string.sub(WStringToString(GameData.ChatData.text), 0, pos-1)
			end		
		end	

		if SystemData.Settings.Language.active == SystemData.Settings.Language.FRENCH then
			local pos = string.find(WStringToString(GameData.ChatData.text), " n'est pas en jeu,")
			if (pos) then
				user = string.sub(WStringToString(GameData.ChatData.text), 0, pos-1)
			end		
		end		
		
		if SystemData.Settings.Language.active == SystemData.Settings.Language.ITALIAN then
			local pos = string.find(WStringToString(GameData.ChatData.text), " non � in gioco")
			if (pos) then
				user = string.sub(WStringToString(GameData.ChatData.text), 0, pos-1)
			end		
		end				
		
		---
		
		if (user ~= nil) then
			for key,obj in ipairs(warwhisperer.conversations) do
				if(obj.name==StringToWString(user)) then
					TextLogAddEntry("WarWhisperer"..user, 3, warwhisperer.GetTimeStamp() .. GameData.ChatData.text)   
					break
				end		
			end
		end
		
	   end
	   
	   -- TELL_RECEIVE
	   
       if( GameData.ChatData.type == SystemData.ChatLogFilters.TELL_RECEIVE ) then
			 if (GameData.ChatData.name ~= L"") then
				warwhisperer.AddConversation(GameData.ChatData.name)
				TextLogAddEntry("WarWhisperer"..WStringToString(GameData.ChatData.name), 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(GameData.ChatData.name)..warwhisperer.textfilter(GameData.ChatData.text))    
				
				warwhisperer.ShowChat(nil)
				warwhisperer.MarkTabForUser(GameData.ChatData.name)
			
			else
					
				--- afk messages comes a bit bad
				local found_chat = false
				for key,obj in ipairs(warwhisperer.conversations) do
					local pos=warwhisperer.getPosition(obj.name,GameData.ChatData.text)
					if(pos==2) then
							local text = string.sub(WStringToString(GameData.ChatData.text), string.len("["..WStringToString(name).."]")+1)
							d(GameData.ChatData.text)
							d(name)
							TextLogAddEntry("WarWhisperer"..WStringToString(name), 1,  warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(name)..warwhisperer.textfilter(StringToWString(text))) 
							found_chat = true
							break
					end
				end		
				if(found_chat == false) then 
					warwhisperer.AddConversation(L"")
					TextLogAddEntry("WarWhisperer", 1, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(L"")..warwhisperer.textfilter(GameData.ChatData.text))    
					warwhisperer.ShowChat(nil)
					warwhisperer.MarkTabForUser(L"")	
				end
			end
       end
	   
	   -- CSR RECEIVE
       if( GameData.ChatData.type == 3 ) then
			warwhisperer.AddConversation(GameData.ChatData.name)
			TextLogAddEntry("WarWhisperer"..WStringToString(GameData.ChatData.name), 4, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(GameData.ChatData.name,L"CSR")..warwhisperer.textfilter(GameData.ChatData.text))    
			warwhisperer.ShowChat(nil)
			warwhisperer.MarkTabForUser(GameData.ChatData.name)
       end
	   	   
	   -- TELL_SEND
       if( GameData.ChatData.type == SystemData.ChatLogFilters.TELL_SEND ) then
			 warwhisperer.AddConversation(GameData.ChatData.name)
			 TextLogAddEntry("WarWhisperer"..WStringToString(GameData.ChatData.name), 2, warwhisperer.GetTimeStamp() .. warwhisperer.GetPlayerLink(GameData.Player.name)..warwhisperer.textfilter(GameData.ChatData.text) )  
			 warwhisperer.ShowChat(GameData.ChatData.name)			 
       end
	   
	   -- check special channels
	   for key,obj in pairs(warwhisperer_defaultchannels) do
			if (WarWhisperer.Settings.channelsdisplay[obj.name] ~= nil) and (WarWhisperer.Settings.channelsdisplay[obj.name] == true) then
				for filterkey, filter in pairs(obj.inputFilter) do
					if( GameData.ChatData.type == filter ) then
						 warwhisperer.AddConversation(L"<"..obj.title.. L">",99,obj)

                         local channame = L""
                         local inputnum = table.getn(obj.inputFilter)	
                         if (inputnum > 1) then
                            channame = L"[" .. warwhisperer.shortName(ChatSettings.Channels[filter].name) .. L"]"
                         end
                         
						 if (GameData.ChatData.name == L"") then
							TextLogAddEntry("InternalWarWhisperer"..obj.name, filter, warwhisperer.GetTimeStamp()..channame..warwhisperer.textfilter(GameData.ChatData.text))
						 else
							TextLogAddEntry("InternalWarWhisperer"..obj.name, filter, warwhisperer.GetTimeStamp()..channame..warwhisperer.GetPlayerLink(GameData.ChatData.name)..warwhisperer.textfilter(GameData.ChatData.text))
						 end
						 
						 warwhisperer.ShowChat()
						 if (obj.flash) then
							warwhisperer.MarkTabForUser(L"<"..obj.title..L">")
						 end
					end				
				end
			end
	   end	

	
end

function warwhisperer.getPosition(name,text) 
            local cleanName=warwhisperer.FixName(name)
			local cleanText=warwhisperer.FixName(text)
			local pos = -1
			if(name ~= L"" ) then 
					  pcall (
					    function ()
					       pos = string.find(WStringToString(GameData.ChatData.text), "["..WStringToString(name).."]")
					       d("Found in positon"..pos)
					   end
					  )
			end
			return pos
					

end 

function warwhisperer.FixName( name )
	-- currently the game adds a ^M or ^F at the end of names for what I 
	-- assume is to determine sex in combat or other messages
	-- it's terrible and it makes comparing names difficult

	if ( name == "" or name == nil ) then
		return L""
	end

	if ( type(name) ~= "wstring" ) then
		name = towstring(name)
	end

	-- this is to prevent unnecessary checks and because wstring.reverse(L"") returns a STRING, and that breaks stuff
	if ( name == L"" ) then
		return name
	end

	if ( wstring.find(name, L"^", 1, true) ~= nil ) then
		name = wstring.sub(name, 1, wstring.find(name, L"^", 1, true)-1)
	end

	-- remove spaces at the start and end of names. IE: I encounted a L"Rock-Biter Gnoblar " that compared incorrectly.
	-- OR TAB CHARACTERS (WHAT THE FUCK, MYTHIC?) "Battle for Praag" in other languages has a TAB CHARACTER in it.
	-- AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!
	local done = false
	while ( not done ) do
		if ( wstring.find(name, L" ", 1, true) == 1 or wstring.find(name, L"	", 1, true) == 1) then
			name = wstring.sub(name, 2, -1)
		elseif ( wstring.find(wstring.reverse(name), L" ", 1, true) == 1 or wstring.find(wstring.reverse(name), L"	", 1, true) == 1 ) then
			name = wstring.sub(name, 1, -2)
		else
			done = true
		end
	end

	return name
end

local function filterurl(theURL)
    if(type(theURL) ~= "string" or theURL == "") then
        return ""
    else
		local link  = CreateHyperLink( L"WARWHISPERER:URLLINK:"..StringToWString(theURL), StringToWString(theURL), {}, {} )	
		link = link
		return " "..WStringToString(link)
    end
end

function warwhisperer.textfilter(text)
	--urlpatterns
	text = WStringToString(text)
	for i=1, table.getn(urlpatterns) do
		text = string.gsub(text, urlpatterns[i], filterurl)
	end 

	return StringToWString(text)
end

function warwhisperer.cleantextfilter(text)
	return text
end

function warwhisperer.OnHyperLinkLButtonUp( linkData, flags, x, y )

    -- URL Links
    local url, findCount = wstring.gsub( linkData, L"WARWHISPERER:URLLINK:", L"" )
    if( findCount > 0  ) 
    then       
		W_URLCOPY.TextBox:SetText(url)
		W_URLCOPY:Show()
       return
    end
	
	--default
	EA_ChatWindow.OnHyperLinkLButtonUp( linkData, flags, x, y )
end
